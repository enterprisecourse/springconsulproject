== SpringBootConsulProject
    - конфигурация в консул
    - инфраструктура в проекте
    - миграции через liuibase

 источник
https://lms.ithillel.ua/groups/5f79e89d4a52327cb26670c1/homeworks

== Migrations
run migration
```bash
mvn liquibase:update -Ddb.user.name=<user name> -Ddb.user.pass=<user password> -Ddb.url=<db url>
```

== Configuration
```yaml
#spring:
#  datasource:
#    url: <db-url>
#    username: <db-user>
#    password: <db-password>
#    driver-class-name: org.postgresql.Driver
#    hikari:
#      maximum-pool-size: 10
#      minimum-idle: 5
```


