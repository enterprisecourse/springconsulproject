package com.example.springbootconsul.entiti;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;

@Accessors(chain = true)
@Data
@Entity
@Table(name = "notes")

public class Note {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_note")
    private Long id;
    @Column(name = "notes_name")
    private String name;
    @Column(name = "notes_status")
    private int status;
    @Column(name = "notes_description")
    private String description;


    @OneToOne()
    @JoinColumn(name = "id_users")
    private User user;





}
