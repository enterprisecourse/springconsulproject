package com.example.springbootconsul.dto;


import lombok.Data;
import lombok.experimental.Accessors;

@Accessors
@Data
public class ResponseDtoPage {

    private String pageNumber;

}
