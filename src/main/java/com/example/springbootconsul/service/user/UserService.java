package com.example.springbootconsul.service.user;

import com.example.springbootconsul.entiti.User;
import com.example.springbootconsul.repositories.user.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class UserService implements MainServiceUser<User> {


    @Autowired
    UserRepository repositoryUser;


    @Override
    public void addUserDataBaseService(User user) {

        repositoryUser.addUserDataBase(user);
    }

    @Override
    public User findUserByLogin(String login) throws Exception {

        return repositoryUser.findUserByLogin(login);
    }
}
