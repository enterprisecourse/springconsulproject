package com.example.springbootconsul.service.user;



public interface MainServiceUser <User> {


    public void addUserDataBaseService(User user);


    User findUserByLogin(String login) throws Exception;
}
