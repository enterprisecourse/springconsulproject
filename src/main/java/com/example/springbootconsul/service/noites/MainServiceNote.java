package com.example.springbootconsul.service.noites;

import org.springframework.data.domain.Page;

import java.util.List;

public interface MainServiceNote <User,Note> {


    public List<Note> getAllNotesByUserLogin(User user);

    List<Note> getAllNotesActiveByUserLogin(User user);

    public void publicationNote(Note note, String nameNote);

    void addNote(Note note, User user);

    Note findNoteByLogin(String notesName);

    public Page getPage(Integer pageNumber);

    void findNotesStartWith(String noteStartWith);
}
