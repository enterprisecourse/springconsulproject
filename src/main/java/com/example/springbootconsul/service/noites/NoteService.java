package com.example.springbootconsul.service.noites;


import com.example.springbootconsul.entiti.Note;
import com.example.springbootconsul.entiti.User;
import com.example.springbootconsul.repositories.note.MainNoteRepository;
import com.example.springbootconsul.repositories.note.jparepo.NoteRepositoryJPA;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class NoteService implements MainServiceNote<User, Note> {


    @Autowired

    MainNoteRepository noteRepository;


    private final NoteRepositoryJPA noteRepositoryJPA;



    @Override
    public List<Note> getAllNotesByUserLogin(User user) {

        return noteRepository.getAllNotesByUserLogin(user);

    }

    @Override
    public List<Note> getAllNotesActiveByUserLogin(User user) {

        return noteRepository.getAllNotesActiveByUserLogin(user);

    }

    @Override
    public void publicationNote(Note note, String nameNote) {
        noteRepository.publicationNote(note,nameNote);

    }

    public void deleteNotes(String noteName) {
        noteRepository.deleteNotes(noteName);
    }

    public void editNote(Note note) {
        noteRepository.editNote(note);
    }

    public void addNote(Note note, User user) {
        noteRepository.addNote(note,user);
    }



    @Override
    public Note findNoteByLogin(String notesName) {
        return noteRepository.findNoteByLogin(notesName);
    }




    public Page getPage(Integer pageNumber) {

        int PAGE_SIZE = 3;

        return noteRepositoryJPA.findAll(PageRequest.of(pageNumber,PAGE_SIZE));


    }

    @Override
    public void findNotesStartWith(String noteStartWith) {

        Note note = (Note) noteRepositoryJPA.findByNameStartsWith(noteStartWith);

    }


}
