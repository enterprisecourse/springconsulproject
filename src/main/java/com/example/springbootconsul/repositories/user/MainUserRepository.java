package com.example.springbootconsul.repositories.user;


public interface MainUserRepository <User,String> {



    public void addUserDataBase(User user);

    User findUserByLogin(String login) throws Exception;
}
