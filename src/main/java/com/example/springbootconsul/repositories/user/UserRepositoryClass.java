package com.example.springbootconsul.repositories.user;

import com.example.springbootconsul.entiti.User;
import lombok.RequiredArgsConstructor;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@RequiredArgsConstructor
@Repository
public class UserRepositoryClass implements UserRepository{

    private final SessionFactory sessionFactory;

    @Override
    public void addUserDataBase(User user) {

        Session session = sessionFactory.openSession();
        session.getSession().save(user);

    }

    @Override
    public User findUserByLogin(String login) throws Exception {
        String log = login;
        Session session = sessionFactory.openSession();
        Optional<User> optional = session.createQuery("SELECT v FROM User v where v.login=:log", User.class)
                .setParameter("log", log).list().stream().findFirst();

        if(optional.isPresent()){
            User user1 = optional.get();
            return user1;
        }else {
            throw new Exception("return new User().getLogin(); null");
        }


    }


}
