package com.example.springbootconsul.repositories.note.jparepo;

import com.example.springbootconsul.entiti.Note;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface NoteRepositoryJPA extends JpaRepository<Note,String> {


    public List<Note> findByNameStartsWith(String nameNoteStart);



}
