package com.example.springbootconsul.repositories.note;


import com.example.springbootconsul.entiti.Note;

import java.util.List;

public interface MainNoteRepository<User,String> {


    public List<Note> getAllNotesByUserLogin(User user);
    public List<Note> getAllNotesActiveByUserLogin(User user);


    void publicationNote(Note note, String nameNote);

    void deleteNotes(String notesName);

    void editNote(Note note);

    void addNote(Note note, User user);

    Note findNoteByLogin(String notesName);
}
