package com.example.springbootconsul.repositories.note;

import com.example.springbootconsul.entiti.Note;
import com.example.springbootconsul.entiti.User;
import lombok.RequiredArgsConstructor;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Repository
public class NoteRepositoryClass implements MainNoteRepository<User, String> {

    private final SessionFactory sessionFactory;
    private final EntityManager entityManager;

    @Override
    public List<Note> getAllNotesByUserLogin(User user) {

        String login = user.getLogin();

        Session session = sessionFactory.openSession();


        List<Note> noteList = session.createQuery("SELECT v FROM Note v where v.user.login=:login", Note.class)
                .setParameter("login", login).list().stream().collect(Collectors.toList());


        return noteList;

    }

    @Override
    public List<Note> getAllNotesActiveByUserLogin(User user) {

        String login = user.getLogin();

        Session session = sessionFactory.openSession();
        List<Note> list = new ArrayList<>();


        List<Note> noteList = session.createQuery("SELECT v FROM Note v where v.user.login=:login", Note.class)
                .setParameter("login", login).list().stream().collect(Collectors.toList());


        for (Note note : noteList) {
            if (note.getStatus() == 2) {
                list.add(note);
            }
        }
        return list;

    }

    @Override
    public void publicationNote(Note note, String nameNote) {

        Session session = sessionFactory.openSession();
        Transaction tran = session.beginTransaction();
        String names = nameNote;
        Session sessi = sessionFactory.openSession();

        List<Note> notes = session.createQuery("SELECT v FROM Note v where v.name=:names", Note.class)
                .setParameter("names", names).list().stream().collect(Collectors.toList());

        Note not = notes.get(0);
        not.setStatus(2);
        session.remove(not);
        session.save(not);
        tran.commit();

    }


    @Override
    public void deleteNotes(String nameNote) {

        Session session = sessionFactory.openSession();
        Transaction tran = session.beginTransaction();
        String names = nameNote;
        Session sessi = sessionFactory.openSession();

        List<Note> notes = session.createQuery("SELECT v FROM Note v where v.name=:names", Note.class)
                .setParameter("names", names).list().stream().collect(Collectors.toList());

        Note not = notes.get(0);
        not.setStatus(3);
        session.remove(not);
        tran.commit();


    }

    @Override
    public void editNote(Note note) {

        String namesNote = note.getName();
        String descriptionNew = note.getDescription();
        Session session = sessionFactory.openSession();
        Transaction tran = session.beginTransaction();

        List<Note> notes = session.createQuery("SELECT v FROM Note v where v.name=:namesNote", Note.class)
                .setParameter("namesNote", namesNote).list().stream().collect(Collectors.toList());

        Note not = notes.get(0);
        not.setDescription(descriptionNew);
        not.setStatus(3);
        session.remove(not);
        session.save(not);
        tran.commit();

    }

    @Override
    public void addNote(Note note, User user) {

        note.setUser(user);


        Session session = sessionFactory.openSession();
        session.getSession().save(note);


    }

    @Override
    public Note findNoteByLogin(String notesName) {

        String names = notesName;
        Session session = sessionFactory.openSession();

        List<Note> notes = session.createQuery("SELECT v FROM Note v where v.name=:names", Note.class)
                .setParameter("names", names).list().stream().collect(Collectors.toList());

        Note note = notes.get(0);

        return note;


    }



}
