package com.example.springbootconsul.controller;



import com.example.springbootconsul.dto.ResponseDto;
import com.example.springbootconsul.dto.ResponseDtoUser;
import com.example.springbootconsul.entiti.Note;
import com.example.springbootconsul.entiti.User;
import com.example.springbootconsul.repositories.note.jparepo.NoteRepositoryJPA;
import com.example.springbootconsul.service.noites.NoteService;
import com.example.springbootconsul.service.user.MainServiceUser;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor
public class NotesController {

    @Autowired
    NoteService noteService;  // сервис для работы c пользоватенлями
    @Autowired
    MainServiceUser serviceUser;  // сервис для работы c пользоватенлями

    @Autowired
    NoteRepositoryJPA noteRepositoryJPA;


    @PostMapping("addNote") // API добавить пользователя
    public void addNote(@RequestBody ResponseDto responseDto) throws Exception {

        User user = (User) serviceUser.findUserByLogin(responseDto.getLogin());

        Note note = new Note().setName(responseDto.getNotesName()).setDescription(responseDto.getNotesDescription()).setStatus(1);

        noteService.addNote(note,user);

    }

    @PostMapping("findNotesByUser") // API добавить пользователя
    public List<Note> findNotesByUser(@RequestBody ResponseDto responseDto) throws Exception {
        User user = (User) serviceUser.findUserByLogin(responseDto.getLogin());
        List<Note> noteList = noteService.getAllNotesByUserLogin(user);
        return noteList;
    }

    @PostMapping("findNotesActiveByUser") // API добавить пользователя
    public List<Note> findNotesAllByUser(@RequestBody ResponseDtoUser responseDto) throws Exception {

        User user = (User) serviceUser.findUserByLogin(responseDto.getLogin());
        List<Note> noteList = noteService.getAllNotesActiveByUserLogin(user);
        return noteList;
    }

    @PostMapping("publicationNote") // API добавить пользователя
    public void findNotesAllByUser(@RequestBody ResponseDto responseDto) throws Exception {

        String nameNote = responseDto.getNotesName();

        Note note = noteService.findNoteByLogin(responseDto.getNotesName());

        noteService.publicationNote(note,nameNote);

    }

    @PostMapping("deleteNote") // API добавить пользователя
    public void pdeleteNote(@RequestBody ResponseDto responseDto) throws Exception {

        String nameNote = responseDto.getNotesName();
        noteService.deleteNotes(nameNote);

    }

    @PostMapping("editNote") // API добавить пользователя
    public void editNote(@RequestBody ResponseDto responseDto) throws Exception {

        String nameNote = responseDto.getNotesName();
        Note note = new Note().setName(responseDto.getNotesName()).setDescription(responseDto.getNotesDescription());

        noteService.editNote(note);

    }

    @GetMapping("{page}")
    public List<Note>  getPageNote(@PathVariable(value = "page",required = false) Integer pagenumber) throws Exception {

        Page<Note> noteList = noteService.getPage(pagenumber);

        List<Note> noteList1 = noteList.toList();

        return noteList1;

    }


    @PostMapping("findNotesStartWith") // API добавить пользователя
    public List<Note> findNotesStartWith(@RequestBody ResponseDto responseDto) throws Exception {

        String noteStartWith = responseDto.getNotesName();
        System.out.println("noteStartWith "+noteStartWith);

        return noteRepositoryJPA.findByNameStartsWith(noteStartWith);

    }



}
